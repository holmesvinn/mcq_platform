// application module imports
const quiz = require("./routes/quiz");
const users = require("./routes/users");
const topics = require("./routes/topics");

// module imports
const path = require("path");
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");

// mongodb connection
const uri = process.env.MONGO_URI;
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
mongoose.Promise = global.Promise;

const app = express(); //init express method
app.use(cors());
app.use(express.json()); //hook body parser via express.json
app.use(express.static("build")); //serve static dist filds

app.get("/ping", (req, res) => res.send({ msg: "pong" }));
//hook all routes for the application
app.use("/api/topics", topics);
app.use("/api/quiz", quiz);
app.use("/api/users", users);

app.listen(process.env.PORT, () => {
  console.log("application started");
});
