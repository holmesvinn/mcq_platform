// express router imports
const express = require("express");
const router = express.Router();

// mongoose related imports
const mongoose = require("mongoose");
const { userSchema, TopicSchema, QuizSchema } = require("../schemas/appSchema");
const User = mongoose.model("users", userSchema);
const Topic = mongoose.model("topics", TopicSchema);
const Quiz = mongoose.model("quiz", QuizSchema);

const getAllModels = (userID) => {
  const user = new User({
    userId: userID,
  });

  const topic = new Topic({
    userId: userID,
    topics: [],
  });

  const quiz = new Quiz({
    userId: userID,
    quiz: [],
  });

  return { user, topic, quiz };
};

// updates the userInformation data
router.post("/addUser", function (req, res) {
  const userID = req.body.userId;
  if (userID) {
    User.findOne({ userId: userID }, (err, data) => {
      if (err) res.send({ error: JSON.stringify(error) });
      else if (data) {
        res.send({ isNewUser: false, result: "User Already exists" });
      } else {
        const { user, topic, quiz } = getAllModels(req.body.userId);
        try {
          topic.save();
          user.save();
          res.send({ isNewUser: true, result: "Successfully added new user" });
        } catch (e) {
          res.send({ error: "some Error Occurred" });
        }
      }
    });
  } else {
    res.send({ error: "userId is not mandatory" });
  }
});

module.exports = router;
