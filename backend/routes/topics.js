const { json } = require("body-parser");
const express = require("express");
const router = express.Router();

const mongoose = require("mongoose");
const { TopicSchema, QuizSchema } = require("../schemas/appSchema");
const Topics = new mongoose.model("topics", TopicSchema);
const Quiz = new mongoose.model("quiz", QuizSchema);

// gets the list of all topics for the given userID
router.get("/getAllTopics", function (req, res) {
  const userID = req.query.userId;
  if (!userID) res.send({ error: "userId is mandatory" });
  else {
    Topics.findOne({ userId: userID }, (err, data) => {
      if (err) res.send({ error: JSON.stringify(err) });
      else if (data) {
        res.send({ ...data._doc });
      } else {
        res.send({ error: "No data for the user" });
      }
    });
  }
});

// for the given userId, adds the new topic in the topics Array
router.post("/addNewTopic", function (req, res) {
  const userID = req.query.userId;
  const topicID = req.body.topicId;
  console.log(userID, topicID);
  if (!userID) res.send({ error: "userId is mandatory" });
  else if (topicID) {
    Topics.findOne({ userId: userID }, (err, data) => {
      if (err) res.send({ error: err });
      else if (data) {
        if (data.topics.indexOf(topicID) !== -1) {
          res.send({ error: "topic already exists" });
        } else {
          const newQuiz = new Quiz({
            userId: userID,
            topicId: topicID,
            quiz: [],
          });
          Quiz.create(newQuiz).catch((err) => console.error(err));

          Topics.findOneAndUpdate(
            { userId: userID },
            { $push: { topics: topicID } },
            { new: true },
            (err, data) => {
              if (err) res.send({ error: "error adding new topic" });
              else
                res.send({
                  result: "new topic added successfully",
                  ...data._doc,
                });
            }
          );
        }
      } else {
        const newTopic = new Topics({
          userId: userID,
          topics: [],
        });
        newTopic.save().catch((err) => console.error(err));
        res.send({ results: "successfully created new topic" });
      }
    });
  } else {
  }
});

// replaces the oldTopicId with the new Topic ID for the incoming user
router.put("/updateTopic", function (req, res) {
  const userID = req.query.userId;
  const oldID = req.body.oldId;
  const newID = req.body.newId;

  Topics.findOne(
    {
      userId: userID,
    },
    (err, data) => {
      if (err) res.send({ error: "error updating topic" });
      else if (data) {
        if (data._doc.topics.indexOf(newID) !== -1)
          res.send({ error: "newId sent already exists", ...data._doc });
        else {
          let replace = data.topics.findIndex((topic) => topic === oldID);
          let newTopics = [...data._doc.topics];
          newTopics[replace] = newID;

          Quiz.findOneAndUpdate(
            { topicId: oldID },
            { $set: { topicId: newID } },
            { upsert: true }
          ).catch((err) => {
            console.error("error updating new quiz");
          });

          Topics.findOneAndUpdate(
            { userId: userID },
            { $set: { topics: newTopics } },
            { new: true },
            (err, data) => {
              if (err) res.send({ error: "error updating the topic" });
              else
                res.send({
                  results: "topic updated successully",
                  ...data._doc,
                });
            }
          );
        }
      }
    }
  );
});

// deletes the given topicId for the incoming user
router.delete("/deleteTopic/:topicId", function (req, res) {
  const userID = req.query.userId;
  const topicID = req.params.topicId;
  console.log(topicID);

  Topics.findOne({ userId: userID }, (err, data) => {
    if (err) res.send({ error: "topic doesnot exists" });
    else {
      const AllTopics = data._doc.topics;
      const deleteIndex = AllTopics.findIndex((el) => el === topicID);
      console.log(deleteIndex);
      console.log(AllTopics);
      AllTopics.splice(deleteIndex, 1);

      Quiz.findOneAndDelete({ topic: topicID }).catch((err) => {
        console.error("error updating new quiz");
      });

      Topics.findOneAndUpdate(
        { userId: userID },
        { $set: { topics: AllTopics } },
        { new: true },
        (err, data) => {
          if (err) res.send({ error: "error updating the topic" });
          else res.send({ results: "successfully deleted", ...data._doc });
        }
      );
    }
  });
});

module.exports = router;
