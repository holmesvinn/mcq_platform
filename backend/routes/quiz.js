const express = require("express");
const router = express.Router();

const mongoose = require("mongoose");
const { QuizSchema } = require("../schemas/appSchema");
const Quiz = new mongoose.model("quiz", QuizSchema);

// gets all quiz for a specific topic under the user
router.get("/getAllQuiz/:topicId", async function (req, res) {
  const userID = req.query.userId;
  const topicID = req.params.topicId;
  if (!userID) res.send({ error: "userID is mandatory" });
  else {
    const AllQuiz = await Quiz.find({ topicId: topicID, userId: userID }).catch(
      (err) => {
        consol.error(err);
        res.send({ error: "error getting quiz" });
      }
    );
    if (AllQuiz) {
      res.send({ data: { ...AllQuiz } });
    } else {
      res.send({ results: "No data", data: [] });
    }
  }
});

// adds new quiz under the specific topic for the user
router.post("/addNewQuiz", async function (req, res) {
  const userID = req.query.userId;
  const quizQuestion = req.body.question;
  const topicID = req.body.topicId;
  const quizAnswers = req.body.answers;
  console.log(quizAnswers);
  if (!userID || !quizQuestion || !quizAnswers || !topicID)
    res.send({ error: "incoming data is missing" });
  else {
    const result = await Quiz.findOne({
      topicId: topicID,
      userId: userID,
    }).catch((err) => res.send({ error: "error finding topic" }));
    if (result && result.topicId) {
      // add the quiz's({question,answer}) in the quiz array of that topic
      const quizUpdateResult = await Quiz.findOneAndUpdate(
        { topicId: topicID, userId: userID },
        { $push: { quiz: { question: quizQuestion, answers: quizAnswers } } },
        { new: true }
      ).catch((err) =>
        res.send({ error: "error adding new Quiz to existing topic" })
      );
      if (quizUpdateResult)
        res.send({
          results: "updated quiz under the topic",
          ...quizUpdateResult._doc,
        });
    } else {
      // create a new quiz document for the topic
      const newQuizDoc = new Quiz({
        topicId: topicID,
        userId: userID,
        quiz: [{ question: quizQuestion, answers: quizAnswers }],
      });
      const addQuiz = await newQuizDoc.save({ new: true }).catch((err) => {
        console.error(err);
      });
      if (addQuiz)
        res.send({ results: "quiz created successfully", ...addQuiz._doc });
      else res.send({ error: "invalid Information" });
    }
  }
});

router.put("/updateQuiz", async function (req, res) {
  const userID = req.query.userId;
  const topicID = req.body.topicId;
  const quizQuestion = req.body.question;
  const quizAnswers = req.body.answers;
  const updateIndex = req.body.uIndex;
  console.log(userID, topicID, quizQuestion, quizAnswers, updateIndex);
  if (!userID || !topicID || !quizQuestion || !quizAnswers)
    res.send({ error: "data missing" });
  else {
    const quizDoc = await Quiz.findOne({
      userId: userID,
      topicId: topicID,
    }).catch((err) => res.send({ error: "error getting data" }));

    if (quizDoc) {
      console.log(quizDoc);
      const AllQuizzes = quizDoc.quiz;
      if (AllQuizzes[Number(updateIndex)]) {
        AllQuizzes[Number(updateIndex)] = {
          question: quizQuestion,
          answers: quizAnswers,
        };
        const updateQuiz = await Quiz.findOneAndUpdate(
          {
            userId: userID,
            topicId: topicID,
          },
          {
            $set: { quiz: AllQuizzes },
          },
          { new: true }
        );
        res.send({
          results: "successfully updated new Quiz",
          ...updateQuiz._doc,
        });
      } else res.send({ error: "incorrect Index" });
    } else res.send({ results: "given data doesnt match records" });
  }
});

router.delete("/deleteQuiz/:topicId/:index", async function (req, res) {
  const userID = req.query.userId;
  const topicID = req.params.topicId;
  const deleteIndex = req.params.index;

  const isQuizExists = await Quiz.findOne({
    topicId: topicID,
    userId: userID,
  }).catch((err) => console.error(err));
  if (!isQuizExists) res.send({ error: "invalid topic or userID" });
  else {
    const AllQuiz = isQuizExists.quiz;
    console.log(AllQuiz, deleteIndex, AllQuiz[deleteIndex]);
    if (AllQuiz[Number(deleteIndex)]) {
      AllQuiz.splice(deleteIndex, 1);
      const deleteResult = await Quiz.findOneAndUpdate(
        { topicId: topicID, userId: userID },
        { $set: { quiz: AllQuiz } },
        { new: true }
      ).catch((err) => console.error(err));
      res.send({ results: "successfully deleted", ...deleteResult._doc });
    } else res.send({ error: "invalid Index" });
  }
});

module.exports = router;
