const mongoose = require("mongoose");
const { Schema } = mongoose;

const SingleQuestionSchema = new Schema({
  question: String,
  answers: JSON,
});

const QuizSchema = new Schema({
  userId: String,
  topicId: String,
  quiz: [SingleQuestionSchema],
});

const TopicSchema = new Schema({
  userId: String,
  topics: [String],
});

const userSchema = new Schema({
  userId: String,
});

module.exports = {
  userSchema,
  TopicSchema,
  QuizSchema,
};
