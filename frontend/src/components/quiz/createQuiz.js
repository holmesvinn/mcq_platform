// css imports
import "./quiz.css";
import "react-quill/dist/quill.snow.css";

// react imports
import ReactQuill from "react-quill";
import React, { useState } from "react";
import { useDispatch } from "react-redux";

// custom imports
import {
  CreateNewQuizEffect,
  UpdateExistingQuiz,
} from "../../store/middlewares/thunk.effects";

// material imports
import Save from "@material-ui/icons/Save";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

// ******************************************************************************//

// returns the initial state of the question in the react-quill based on edit or new
const getQuizQuestionInitialState = (editCloneData) => {
  if (editCloneData?.clone || editCloneData?.edit) {
    return editCloneData?.data
      ? editCloneData.data?.quiz?.question
      : "your question here";
  } else {
    return "Type Your Question";
  }
};

// returns the Initial state for the selectedAnswer radio button based on edit or new
const getAnswersRadioInitialState = (editCloneData) => {
  if (editCloneData?.edit || editCloneData?.clone) {
    let correctIndex = "0";
    // eslint-disable-next-line no-unused-expressions
    editCloneData?.data?.quiz?.answers?.length
      ? editCloneData.data.quiz.answers.forEach((answer, index) => {
          if (answer.isCorrect) correctIndex = `${index}`;
        })
      : null;
    return correctIndex;
  } else {
    return "0";
  }
};

// returns the initial state of all the answers
const getAnswersInitialState = (editCloneData) => {
  if (editCloneData?.edit || editCloneData?.clone) {
    let ans = {};
    [0, 1, 2, 3].forEach((el) => {
      ans[el] = editCloneData?.data?.quiz?.answers[el]?.answer;
    });
    return ans;
  } else {
    return {};
  }
};

export const ModalBody = (props) => {
  const dispatch = useDispatch();
  const editCloneData = props.editCloneData;
  const [value, setValue] = useState(() =>
    getQuizQuestionInitialState(editCloneData)
  );
  const [answerValue, setAnswerValue] = useState(() =>
    getAnswersRadioInitialState(editCloneData)
  );
  const [answers, setAnswers] = useState(() =>
    getAnswersInitialState(editCloneData)
  );

  // handler for posting, editing a new quiz item
  const hanldeCreateQuiz = () => {
    const userID = props.userId;
    const quizQuestion = value;
    const topicID = props.topicId;
    const quizAnswers = Array(4)
      .fill("")
      .map((_, index) => {
        return {
          answer: answers[index] ? answers[index] : `option ${index + 1}`,
          isCorrect: answerValue === `${index}` ? true : false,
        };
      });

    dispatch(
      editCloneData.edit
        ? UpdateExistingQuiz({
            userID,
            topicID,
            quizQuestion,
            quizAnswers,
            index: editCloneData?.data?.index,
          })
        : CreateNewQuizEffect({ userID, topicID, quizQuestion, quizAnswers })
    );
    props.openClose(false);
  };

  return (
    <>
      <div className="quiz-create-wrapper">
        <div className="quiz-editor-wrapper">
          <ReactQuill theme="snow" value={value} onChange={setValue} />
          <div className="add-answers-wrapper">
            <RadioGroup
              aria-label="gender"
              name="gender1"
              value={answerValue}
              onChange={(e) => setAnswerValue(e.target.value)}
            >
              <div className="single-answer-wrapper">
                <FormControlLabel value="0" control={<Radio />} label="a." />
                <TextareaAutosize
                  onChange={(e) =>
                    setAnswers({ ...answers, 0: e.target.value })
                  }
                  minRows={2}
                  value={answers[0]}
                  className="text-area-answer"
                  placeholder="option 1"
                />
              </div>
              <div className="single-answer-wrapper">
                <FormControlLabel value="1" control={<Radio />} label="b." />
                <TextareaAutosize
                  onChange={(e) =>
                    setAnswers({ ...answers, 1: e.target.value })
                  }
                  minRows={2}
                  value={answers[1]}
                  className="text-area-answer"
                  placeholder="option 2"
                />
              </div>
              <div className="single-answer-wrapper">
                <FormControlLabel value="2" control={<Radio />} label="c." />
                <TextareaAutosize
                  onChange={(e) =>
                    setAnswers({ ...answers, 2: e.target.value })
                  }
                  minRows={2}
                  value={answers[2]}
                  className="text-area-answer"
                  placeholder="option 3"
                />
              </div>
              <div className="single-answer-wrapper">
                <FormControlLabel value="3" control={<Radio />} label="d." />
                <TextareaAutosize
                  onChange={(e) =>
                    setAnswers({ ...answers, 3: e.target.value })
                  }
                  minRows={2}
                  value={answers[3]}
                  className="text-area-answer"
                  placeholder="option 4"
                />
              </div>
            </RadioGroup>
          </div>
        </div>
        <div className="submit-button-wrapper">
          <Button
            className="button-save-quiz"
            startIcon={<Save />}
            color="secondary"
            variant="contained"
            onClick={hanldeCreateQuiz}
          >
            Save
          </Button>
        </div>
      </div>
    </>
  );
};
