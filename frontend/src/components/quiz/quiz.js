// css imports
import "./quiz.css";
// react imports
import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// custom imports
import { ModalBody } from "./createQuiz";
import {
  deleteQuizEffect,
  getAllQuizEffect,
} from "../../store/middlewares/thunk.effects";
import ReactHtmlParser from "react-html-parser";

// material imports
import Card from "@material-ui/core/Card";
import AddIcon from "@material-ui/icons/Add";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import Collapse from "@material-ui/core/Collapse";
import DeleteIcon from "@material-ui/icons/Delete";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import { IconButton, Modal } from "@material-ui/core";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const QuizComponent = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const [editClone, setEditClone] = useState({
    edit: false,
    clone: false,
    data: null,
  });
  const [expanded, setExpanded] = useState({});
  const [openModal, setOpenModal] = useState(false);
  const quizData = useSelector((state) => state.quiz);

  useEffect(() => {
    dispatch(
      getAllQuizEffect({
        userId: params.userId,
        topicId: params.topicId,
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // delete a quiz item handler
  const deleteQuiz = (quizIndex) => {
    dispatch(
      deleteQuizEffect({
        userId: params.userId,
        topicId: params.topicId,
        index: quizIndex,
      })
    );
  };

  // Edit a quiz item handler
  const handleEdit = (quiz, index) => {
    setEditClone({
      edit: true,
      clone: false,
      data: { quiz: quiz, index: index },
    });
    setOpenModal(true);
  };

  // cloning a quiz item handler
  const handleClone = (quiz, index) => {
    setEditClone({
      edit: false,
      clone: true,
      data: { quiz: quiz, index: index },
    });
    setOpenModal(true);
  };

  // handleExpandToggle
  const handleExpandToggle = (index) => {
    setExpanded((prev) => {
      return { ...prev, [index]: !prev[index] };
    });
  };

  return (
    <div className="quiz-section-wrapper">
      <Modal
        className="create-quiz-modal"
        open={openModal}
        onClose={() => setOpenModal(false)}
      >
        <ModalBody
          userId={params.userId}
          topicId={params.topicId}
          openClose={setOpenModal}
          editCloneData={editClone}
        />
      </Modal>
      <div className="add-new-quiz">
        <Button
          color="secondary"
          variant="outlined"
          startIcon={<AddIcon />}
          onClick={() => {
            setEditClone({ edit: false, clone: false, data: null });
            setOpenModal(true);
          }}
        >
          Add Quiz
        </Button>
      </div>
      <div className="quiz-list-wrapper">
        {quizData &&
          quizData.map((quiz, index) => (
            <React.Fragment key={index}>
              <Card className="card-topic-item" variant="outlined">
                <CardContent className="topic-card-content">
                  {ReactHtmlParser(quiz.question)}
                </CardContent>
                <CardActions className="topic-action-buttons">
                  <IconButton
                    size="small"
                    onClick={() => handleEdit(quiz, index)}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    size="small"
                    onClick={() => {
                      deleteQuiz(index);
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton
                    size="small"
                    onClick={() => handleClone(quiz, index)}
                  >
                    <FileCopyIcon />
                  </IconButton>
                  <IconButton onClick={() => handleExpandToggle(index)}>
                    <ExpandMoreIcon />
                  </IconButton>
                </CardActions>

                <Collapse in={expanded[index]}>
                  <Card className="collapsed-answers-quiz">
                    <CardContent>
                      {quiz.answers &&
                        quiz.answers.map((answer, index) => (
                          <div key={index}>
                            <span>
                              {index + 1}. {answer.answer}
                            </span>
                            {answer.isCorrect && <DoneAllIcon />}
                          </div>
                        ))}
                    </CardContent>
                  </Card>
                </Collapse>
              </Card>
            </React.Fragment>
          ))}
      </div>
    </div>
  );
};

export default QuizComponent;
