import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./publish.css";
import { useParams } from "react-router-dom";
import { getAllQuizEffect } from "../../store/middlewares/thunk.effects";
import ReactHtmlParser from "react-html-parser";
import { Button, Modal } from "@material-ui/core";

const getSelectedAnswerCorrect = (quiz, selectedAnswers, index) => {
  const selectedAnswer = selectedAnswers[index];
  const correctAnswer = quiz.answers.findIndex((el) => el.isCorrect === true);
  return correctAnswer === selectedAnswer ? true : false;
};

function PublishResult({ selectedAnswers, quizData }) {
  let marks = 0;
  quizData.forEach((element, index) => {
    const isCorrectAnswerSelected = getSelectedAnswerCorrect(
      element,
      selectedAnswers,
      index
    );
    if (isCorrectAnswerSelected) marks = marks + 1;
  });
  return (
    <div className="published-result">
      Total Marks: {marks}/{quizData.length}
      {marks === 0 ? ` 🤨` : ` 😃`}
    </div>
  );
}

function PublishComponent() {
  const dispatch = useDispatch();
  const params = useParams();
  const [selectedAnswers, setSelectedAnswer] = useState({});
  const [openModal, setOpenModal] = useState(false);
  const quizData = useSelector((state) => state.quiz);

  useEffect(() => {
    dispatch(
      getAllQuizEffect({
        userId: params.userId,
        topicId: params.topicId,
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="publish-component-wrapper">
      <Modal
        className="create-quiz-modal"
        open={openModal}
        onClose={() => setOpenModal(false)}
      >
        <PublishResult selectedAnswers={selectedAnswers} quizData={quizData} />
      </Modal>
      <div className="question-answer-wrapper">
        {quizData &&
          quizData.map((quiz, qindex) => {
            return (
              <div className="single-question-answer-wrapper">
                <div className="question">
                  <span>{qindex + 1}.</span> {ReactHtmlParser(quiz?.question)}{" "}
                </div>
                <div className="answers-list-wrapper">
                  {quiz?.answers?.map((el, aindex) => {
                    return (
                      <div
                        className={
                          selectedAnswers[qindex] === aindex
                            ? "selected-answer single-answer"
                            : "single-answer"
                        }
                        onClick={() => {
                          setSelectedAnswer({
                            ...selectedAnswers,
                            [qindex]: aindex,
                          });
                        }}
                      >
                        {aindex + 1}. {el?.answer}
                      </div>
                    );
                  })}
                </div>
              </div>
            );
          })}
      </div>

      <div className="submit-answer-button">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setOpenModal(true);
          }}
        >
          Submit
        </Button>
      </div>
    </div>
  );
}

export default PublishComponent;
