import React, { useState } from "react";
import { useDispatch } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import "./login.css";
import { updateIncomingUser } from "../../store/middlewares/thunk.effects";

export default function LoginComponent(props) {
  const dispatch = useDispatch();
  const [userName, setUserName] = useState("");
  const handleClick = () => {
    dispatch(updateIncomingUser(userName));
    props.history.push(`/topic/${userName}`);
  };

  return (
    <div className="login-wrapper">
      <div className="user-name-input-wrapper">
        <span> Login to create and publish MCQ's </span>
        <TextField
          label="Enter your UserID"
          variant="outlined"
          color="secondary"
          onChange={(e) => {
            setUserName(e.target.value);
          }}
        />

        <Button
          endIcon={<ArrowForwardIosIcon />}
          variant="contained"
          color="secondary"
          onClick={handleClick}
          disabled={userName === "" ? true : false}
        >
          Enter
        </Button>
      </div>
    </div>
  );
}
