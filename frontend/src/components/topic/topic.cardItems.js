import { Link } from "react-router-dom";
import { Tooltip } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import SendIcon from "@material-ui/icons/Send";
import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";

// *********************************************************************//

// All Action buttons for Topic and its handler -> eidt, delete, publish
export const CardActionButtons = ({
  save,
  index,
  topic,
  userId,
  updateTopic,
  deleteTopic,
  setEditTopics,
}) => {
  // handler for Edit button click
  const editClickHandler = () => {
    if (save) updateTopic();
    setEditTopics((prevObj) => {
      return { ...prevObj, [index]: save ? null : topic };
    });
  };

  return (
    <>
      <Tooltip title="Edit">
        <IconButton
          size="small"
          color="default"
          variant="outlined"
          onClick={editClickHandler}
        >
          {save ? <SaveIcon fontSize="small" /> : <EditIcon fontSize="small" />}
        </IconButton>
      </Tooltip>

      <Tooltip title="Delete">
        <IconButton
          size="small"
          color="default"
          variant="outlined"
          onClick={() => {
            deleteTopic(topic);
          }}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>
      </Tooltip>

      <Tooltip title="Publish">
        <IconButton size="small" color="default" variant="outlined">
          <Link to={`/quiz/${topic}/${userId}/publish`}>
            <SendIcon fontSize="small" />
          </Link>
        </IconButton>
      </Tooltip>
    </>
  );
};

// Editing Card component
export const CardContentComponent = ({
  index,
  topic,
  userId,
  editTopics,
  setEditTopics,
}) => {
  const TopicEditHandler = (value) => {
    setEditTopics({
      ...editTopics,
      [index]: value ? value : " ",
    });
  };
  return (
    <>
      {editTopics[index] ? (
        <TextField
          color="secondary"
          value={editTopics[index]}
          className="edit-text-field"
          onChange={(e) => {
            TopicEditHandler(e.target.value);
          }}
        />
      ) : (
        <Link to={`/quiz/${topic}/${userId}`}>{topic}</Link>
      )}
    </>
  );
};

// component Adds a new Topic
export const AddTopicComponent = ({ setTopicName, addNewTopic }) => {
  return (
    <>
      <TextField
        variant="standard"
        label="Enter the Topic name"
        color="secondary"
        onChange={(e) => setTopicName(e.target.value)}
      />

      <Button
        variant="contained"
        color="secondary"
        startIcon={<SaveIcon />}
        onClick={addNewTopic}
      >
        Add
      </Button>
    </>
  );
};
