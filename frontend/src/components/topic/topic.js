// css imports
import "./topic.css";

// custom Imports
import {
  CardActionButtons,
  CardContentComponent,
  AddTopicComponent,
} from "./topic.cardItems";

import {
  AddNewTopicEffect,
  deleteSingleTopic,
  getAllTopicsEffects,
  upateSingleTopic,
} from "../../store/middlewares/thunk.effects";

// material Imports
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";

// react imports
import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// ****************************************************************************//

const TopicComponent = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const [topicName, setTopicName] = useState("");
  const [editTopics, setEditTopics] = useState({});
  const topics = useSelector((state) => state.topics);

  useEffect(() => {
    dispatch(getAllTopicsEffects(params.userId));
  }, []);

  // handler for adding a new Topic button click
  const addNewTopic = () => {
    dispatch(
      AddNewTopicEffect({ topicName: topicName, userId: params.userId })
    );
  };

  // handler for renaming a topic
  const updateExistingTopic = (index, topic) => {
    dispatch(
      upateSingleTopic({
        userId: params.userId,
        newTopicId: editTopics[index],
        oldTopicId: topic,
      })
    );
  };

  // handler for deleting a topic
  // TODO: add a confirmation message before deleting a topic
  const deleteExistingTopic = (topic) => {
    dispatch(deleteSingleTopic({ userId: params.userId, topicId: topic }));
  };

  return (
    <div className="topics-wrapper">
      <div className="add-topic-wrapper">
        <AddTopicComponent
          setTopicName={setTopicName}
          addNewTopic={addNewTopic}
        />
      </div>
      <div className="topics-list-wrapper">
        {topics &&
          topics.map((topic, index) => (
            <React.Fragment key={topic + index}>
              <Card className="card-topic-item" variant="outlined">
                <CardContent className="topic-card-content">
                  <CardContentComponent
                    topic={topic}
                    setEditTopics={setEditTopics}
                    index={index}
                    userId={params.userId}
                    editTopics={editTopics}
                  />
                </CardContent>
                <CardActions className="topic-action-buttons">
                  <CardActionButtons
                    topic={topic}
                    userId={params.userId}
                    setEditTopics={setEditTopics}
                    index={index}
                    updateTopic={() => updateExistingTopic(index, topic)}
                    deleteTopic={(topic) => deleteExistingTopic(topic)}
                    save={editTopics[index] ? true : false}
                  />
                </CardActions>
              </Card>
            </React.Fragment>
          ))}
      </div>
    </div>
  );
};

export default TopicComponent;
