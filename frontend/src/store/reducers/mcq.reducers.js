import { ActionTypes } from "../actions/mcq.action.types";

export const appInitialState = {
  topics: [],
  quiz: [],
  user: {},
};

export const appReducer = (state = appInitialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.UPDATA_ALL_TOPIC:
      return { ...state, topics: payload };

    case ActionTypes.UPDATE_USER_DATA:
      return { ...state, user: payload };

    case ActionTypes.UPDATE_ALL_QUIZ:
      return { ...state, quiz: payload };

    default:
      return state;
  }
};

export default appReducer;
