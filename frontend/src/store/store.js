import { configureStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import appReducer from "./reducers/mcq.reducers";

const store = configureStore({
  reducer: appReducer,
  middleware: [thunk],
  devTools: true,
});

export default store;
