const BASE_URL = "https://mcq-platform-vignesh.herokuapp.com/";

// Api url for backend controller -> users -> topics -> quiz
export const API_CONFIG = {
  NODE_API_URL_QUIZ: `${BASE_URL}api/quiz`,
  NODE_API_URL_USERS: `${BASE_URL}api/users`,
  NODE_API_URL_TOPICS: `${BASE_URL}api/topics`,
};
