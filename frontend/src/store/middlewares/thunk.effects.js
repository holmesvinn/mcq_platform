import axios from "axios";
import {
  updateAllQuiz,
  updateAllTopics,
  updateUserInfo,
} from "../actions/mcq.actions";
import { API_CONFIG } from "./api.config";

// SideEffect to Get the List of all Topics under the specific user
export const getAllTopicsEffects = (userId) => async (dispatch, _) => {
  const AllTopics = await axios.get(
    `${API_CONFIG.NODE_API_URL_TOPICS}/getAllTopics?userId=${userId}`
  );
  dispatch(updateAllTopics(AllTopics.data.topics));
};

// Add a new User when the user logs in via userID in UI
export const updateIncomingUser = (userId) => async (dispatch, _) => {
  const updateUserResult = await axios.post(
    `${API_CONFIG.NODE_API_URL_USERS}/addUser`,
    { userId: userId }
  );
  dispatch(
    updateUserInfo({
      userId: userId,
      isNewUser: updateUserResult.data.isNewUser,
    })
  );
};

// Add a New Topic to that specific user which will contain multiple quiz
export const AddNewTopicEffect =
  ({ topicName, userId }) =>
  async (dispatch, _) => {
    const newTopicResult = await axios.post(
      `${API_CONFIG.NODE_API_URL_TOPICS}/addNewTopic?userId=${userId}`,
      { topicId: topicName }
    );
    dispatch(updateAllTopics(newTopicResult.data.topics));
  };

// Update the Topic ID when the topic is renamed in the UI
export const upateSingleTopic =
  ({ userId, oldTopicId, newTopicId }) =>
  async (dispatch, getState) => {
    const newUpdatedTopics = await axios.put(
      `${API_CONFIG.NODE_API_URL_TOPICS}/updateTopic?userId=${userId}`,
      { oldId: oldTopicId, newId: newTopicId }
    );
    dispatch(updateAllTopics(newUpdatedTopics.data.topics));
  };

// Delete any one of the topic under the list of topics for the specific user
export const deleteSingleTopic =
  ({ userId, topicId }) =>
  async (dispatch, _) => {
    const deleteTopicsResult = await axios.delete(
      `${
        API_CONFIG.NODE_API_URL_TOPICS
      }/deleteTopic/${topicId.trim()}?userId=${userId}`
    );

    dispatch(updateAllTopics(deleteTopicsResult.data.topics));
  };

// Get the list of all available quiz under a particular topic for a specific user
export const getAllQuizEffect =
  ({ userId, topicId }) =>
  async (dispatch, getState) => {
    const AllQuiz = await axios.get(
      `${
        API_CONFIG.NODE_API_URL_QUIZ
      }/getAllQuiz/${topicId.trim()}?userId=${userId}`
    );
    dispatch(updateAllQuiz(AllQuiz?.data?.data?.[0]?.quiz));
  };

// Delete a specific quiz under a specific topic for a particular user
export const deleteQuizEffect =
  ({ userId, topicId, index }) =>
  async (dispatch, _) => {
    const deleteQuizResult = await axios.delete(
      `${
        API_CONFIG.NODE_API_URL_QUIZ
      }/deleteQuiz/${topicId.trim()}/${index}?userId=${userId}`
    );
    dispatch(updateAllQuiz(deleteQuizResult?.data?.quiz));
  };

// Create a new Quiz Item under a specific topic for the specific user
export const CreateNewQuizEffect =
  ({ userID, topicID, quizQuestion, quizAnswers }) =>
  async (dispatch, _) => {
    const addNewQuizData = await axios.post(
      `${API_CONFIG.NODE_API_URL_QUIZ}/addNewQuiz?userId=${userID}`,
      {
        question: quizQuestion,
        topicId: topicID,
        answers: quizAnswers,
      }
    );
    dispatch(updateAllQuiz(addNewQuizData?.data?.quiz));
  };

// update a quiz item under a specific topic under a specific user
export const UpdateExistingQuiz =
  ({ userID, topicID, quizQuestion, quizAnswers, index }) =>
  async (dispatch, getState) => {
    const updateExistingQuiz = await axios.put(
      `${API_CONFIG.NODE_API_URL_QUIZ}/updateQuiz?userId=${userID}`,
      {
        question: quizQuestion,
        topicId: topicID,
        answers: quizAnswers,
        uIndex: index,
      }
    );
    dispatch(updateAllQuiz(updateExistingQuiz?.data?.quiz));
  };
