import { ActionTypes } from "./mcq.action.types";

export const updateUserInfo = (userData) => {
  return {
    type: ActionTypes.UPDATE_USER_DATA,
    payload: userData,
  };
};

export const updateAllTopics = (topics) => {
  return {
    type: ActionTypes.UPDATA_ALL_TOPIC,
    payload: topics,
  };
};

export const updateAllQuiz = (quizData) => {
  return {
    type: ActionTypes.UPDATE_ALL_QUIZ,
    payload: quizData,
  };
};
