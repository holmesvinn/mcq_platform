export const ActionTypes = {
  UPDATE_USER_DATA: "[user] update the information of the user from the api",

  UPDATA_ALL_TOPIC: "[topics] update the list of all topics bunch",
  ADD_NEW_TOPIC: "[topics] add a new topic for the user",
  EDIT_TOPIC: "[topics] edit the existing topic for the user",
  DELETE_TOPIC: "[topics] delete the existing topic for the user",

  UPDATE_ALL_QUIZ: "[quiz] Update all quiz in a bunch",
  ADD_NEW_QUIZ: "[quiz] Add a new quiz to the list of quiz under th topic",
  EDIT_QUIZ: "[quiz] edit a single quiz from the list of quiz",
  DELETE_QUIZ: "[quiz] delete  a single quiz from the list of quiz",
};
