import "./App.css";
import Header from "./components/header/header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginComponent from "./components/login/login";
import TopicComponent from "./components/topic/topic";
import QuizComponent from "./components/quiz/quiz";
import PublishComponent from "./components/publish/publish";

// Initialising the Routes for the App
function App() {
  const basePath = "/";
  const topicPath = "/topic/:userId";
  const quizComponentPath = "/quiz/:topicId/:userId";
  const publishPath = "/quiz/:topicId/:userId/publish";

  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path={basePath} exact component={LoginComponent} />
          <Route path={topicPath} exact component={TopicComponent} />
          <Route path={quizComponentPath} exact component={QuizComponent} />
          <Route path={publishPath} exact component={PublishComponent} />
          <Route> 404 Not Found </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
